with years as (
	select y, y % 4 = 0 and y % 100 <> 0 or y % 400 = 0 as isLeapYear
	from (
		select unnest(array[2019, 2018, 2017, 2016]) as y
	)years
), dates as (
	select make_date(y, 1, 1) + generate_series as "date"
	from years, generate_series(0, case when isLeapYear then 365 else 364 end)
), extractedDates as (
	select
		extract(year from dates.date) "year",
		to_char(dates.date, 'MONTH') "month",
		extract(month from dates.date) "monthNum",
		extract(dow from dates.date) "dow",
		extract(week from dates.date) "week",
		extract(day from dates.date)::integer "day"
	from dates
), concatedMonths as (
	select
		"year",
		"week",
		"monthNum",
		concat("year", E'\n', "month", E'\n' ,E'Неделя\tПН\tВТ\tСР\tЧТ\tПТ\tСБ\tВС') as title,
		concat(
			"week",
			E'\t',
			max(case when "dow" = 1 then "day" else null end),
			E'\t',
			max(case when "dow" = 2 then "day" else null end),
			E'\t',
			max(case when "dow" = 3 then "day" else null end),
			E'\t',
			max(case when "dow" = 4 then "day" else null end),
			E'\t',
			max(case when "dow" = 5 then "day" else null end),
			E'\t',
			max(case when "dow" = 6 then "day" else null end),
			E'\t',
			max(case when "dow" = 0 then "day" else null end),
			E'\n'
		) calendarStrings
	from extractedDates
	group by "year",
		"monthNum",
		"week",
		title
), calendar as (
	select
		"year",
		concat(title, E'\n\n', string_agg(calendarStrings, '' order by "week" > 50 desc, "week")) monthCalendar,
		"monthNum"::integer
	from concatedMonths
	group by "year", title, "monthNum"
)
select
	max(case when "monthNum" % 3 = 1 then monthCalendar else null end),
	max(case when "monthNum" % 3 = 2 then monthCalendar else null end),
	max(case when "monthNum" % 3 = 0 then monthCalendar else null end)
from calendar
group by "year", ceil("monthNum"::numeric / 3::numeric)
order by "year", ceil("monthNum"::numeric / 3::numeric)
